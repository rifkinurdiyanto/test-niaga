resource "aws_internet_gateway" "niaga-igw" {
  vpc_id = aws_vpc.niaga-test.id

  tags = {
    Name = "niaga-igw"
  }
}