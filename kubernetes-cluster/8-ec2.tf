
resource "aws_instance" "niaga-instance" {
  ami                         = "ami-04ff9e9b51c1f62ca"
  associate_public_ip_address = "true"
  disable_api_termination     = false
  key_name                    = "EC2Niaga"
  instance_type               = "t3.medium"
  subnet_id                   = aws_subnet.niaga-subnet-public1-ap-southeast-1a.id

  root_block_device {
    volume_size           = 50
    volume_type           = "gp2"
    delete_on_termination = true
  }

  vpc_security_group_ids = [
    aws_security_group.niaga-sg.id
  ]

  tags = {
    Name = "niaga-jenkins"
  }
}

resource "aws_security_group" "niaga-sg" {
  name        = "niaga"
  description = "Seurity Rule for niaga Test"
  vpc_id      = aws_vpc.niaga-test.id

  ingress {
    description = "Allow Jenkins Access form Anywhere"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "Allow SSH Access from Anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "niaga"
  }
}