resource "aws_subnet" "niaga-subnet-private1-ap-southeast-1a" {
  vpc_id            = aws_vpc.niaga-test.id
  cidr_block        = "192.168.1.0/26"
  availability_zone = "ap-southeast-1a"

  tags = {
    "Name"                            = "niaga-subnet-private1-ap-southeast-1a"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/EKSNiaga"  = "shared"
  }
}

resource "aws_subnet" "niaga-subnet-private2-ap-southeast-1b" {
  vpc_id            = aws_vpc.niaga-test.id
  cidr_block        = "192.168.1.64/26"
  availability_zone = "ap-southeast-1b"

  tags = {
    "Name"                            = "niaga-subnet-private2-ap-southeast-1b"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/EKSNiaga"  = "shared"
  }
}

resource "aws_subnet" "niaga-subnet-public1-ap-southeast-1a" {
  vpc_id                  = aws_vpc.niaga-test.id
  cidr_block              = "192.168.1.128/26"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    "Name"                           = "niaga-subnet-public1-ap-southeast-1a"
    "kubernetes.io/role/elb"         = "1"
    "kubernetes.io/cluster/EKSNiaga" = "shared"
  }
}

resource "aws_subnet" "niaga-subnet-public2-ap-southeast-1b" {
  vpc_id                  = aws_vpc.niaga-test.id
  cidr_block              = "192.168.1.192/26"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    "Name"                           = "niaga-subnet-public2-ap-southeast-1b"
    "kubernetes.io/role/elb"         = "1"
    "kubernetes.io/cluster/EKSNiaga" = "shared"
  }
}
