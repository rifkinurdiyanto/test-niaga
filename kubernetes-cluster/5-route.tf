resource "aws_route_table" "niaga-private-route-table" {
  vpc_id = aws_vpc.niaga-test.id

  tags = {
    Name = "niaga-private-route-table"
  }
}

resource "aws_route_table" "niaga-public-route-table" {
  vpc_id = aws_vpc.niaga-test.id

  tags = {
    Name = "niaga-public-route-table"
  }
}

resource "aws_route" "niaga-private" {
  route_table_id         = aws_route_table.niaga-private-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.niaga-nat1.id
  depends_on             = [aws_route_table.niaga-private-route-table]
}

resource "aws_route" "niaga-public" {
  route_table_id         = aws_route_table.niaga-public-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.niaga-igw.id
  depends_on             = [aws_route_table.niaga-public-route-table]
}

resource "aws_main_route_table_association" "niaga-test" {
  vpc_id         = aws_vpc.niaga-test.id
  route_table_id = aws_route_table.niaga-private-route-table.id
}

resource "aws_route_table_association" "niaga-subnet-private1-ap-southeast-1a" {
  subnet_id      = aws_subnet.niaga-subnet-private1-ap-southeast-1a.id
  route_table_id = aws_route_table.niaga-private-route-table.id
}

resource "aws_route_table_association" "niaga-subnet-private2-ap-southeast-1b" {
  subnet_id      = aws_subnet.niaga-subnet-private2-ap-southeast-1b.id
  route_table_id = aws_route_table.niaga-private-route-table.id
}

resource "aws_route_table_association" "niaga-subnet-public1-ap-southeast-1a" {
  subnet_id      = aws_subnet.niaga-subnet-public1-ap-southeast-1a.id
  route_table_id = aws_route_table.niaga-public-route-table.id
}

resource "aws_route_table_association" "niaga-subnet-public2-ap-southeast-1b" {
  subnet_id      = aws_subnet.niaga-subnet-public2-ap-southeast-1b.id
  route_table_id = aws_route_table.niaga-public-route-table.id
}