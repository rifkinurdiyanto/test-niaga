resource "aws_eip" "niaga-nat1" {
  vpc = true

  tags = {
    Name = "niaga-nat1"
  }
}

resource "aws_nat_gateway" "niaga-nat1" {
  allocation_id = aws_eip.niaga-nat1.id
  subnet_id     = aws_subnet.niaga-subnet-public1-ap-southeast-1a.id

  tags = {
    Name = "niaga-nat1"
  }

  depends_on = [aws_internet_gateway.niaga-igw]
}