resource "aws_iam_role" "EKSNiaga" {
  name = "eks-cluster-EKSNiaga"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "EKSNiaga-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.EKSNiaga.name
}

resource "aws_eks_cluster" "EKSNiaga" {
  name     = "EKSNiaga"
  role_arn = aws_iam_role.EKSNiaga.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.niaga-subnet-private1-ap-southeast-1a.id,
      aws_subnet.niaga-subnet-private2-ap-southeast-1b.id,
      aws_subnet.niaga-subnet-public1-ap-southeast-1a.id,
      aws_subnet.niaga-subnet-public2-ap-southeast-1b.id
    ]

    endpoint_private_access = false
    endpoint_public_access  = true
  }

  depends_on = [aws_iam_role_policy_attachment.EKSNiaga-AmazonEKSClusterPolicy]
}
